package com.binygal.core.messenger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Binyamin on 3/10/2015.
 */
public class Messenger {
    private Messenger(){
        registrators = new ArrayList<MessengerRegistrationData>();
    }

    private static Messenger instance;

    public static Messenger getInstance(){
        if (instance == null){
            instance = new Messenger();
        }
        return instance;
    }

    public void register(Object registrator, MessengerListener listener, String message){
        MessengerRegistrationData data = new MessengerRegistrationData(registrator, listener, message);
        registrators.add(data);
    }

    public void unregister(Object registrator){
        unregister(registrator, null);
    }

    public void send(String message){
        for (MessengerRegistrationData data : registrators){
            if (message.equals(data.getMessage())){
                data.execute(null);
            }
        }
    }

    public void unregister(Object registrator, String message){
        ArrayList<MessengerRegistrationData> list = new ArrayList<MessengerRegistrationData>();
        for (MessengerRegistrationData data : registrators){
            if (registrator.equals(data.getRegisterdObject())){
                if (message == null || message.equals(data.getMessage())) {
                    list.add(data);
                }
            }
        }
        for (MessengerRegistrationData d: list){
            registrators.remove(d);
        }
    }

    List<MessengerRegistrationData> registrators;
}
