package com.binygal.core.messenger;

/**
 * Created by Binyamin on 3/10/2015.
 */
public interface MessengerListener<E> {
    void OnMessageArrived(E messageData);
}
