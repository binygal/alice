package com.binygal.core.messenger;

/**
 * Created by Binyamin on 3/10/2015.
 */
public class MessengerRegistrationData {
    public MessengerRegistrationData(Object registerdObject, MessengerListener listener, String message){
        this.registerdObject = registerdObject;
        this.listener = listener;
        this.message = message;
    }

    private Object registerdObject;

    public MessengerListener getListener() {
        return listener;
    }

    public String getMessage(){
        return message;
    }

    public Object getRegisterdObject() {
        return registerdObject;
    }

    private MessengerListener listener;

    private String message;

    public void execute(Object o) {
        getListener().OnMessageArrived(o);
    }
}
