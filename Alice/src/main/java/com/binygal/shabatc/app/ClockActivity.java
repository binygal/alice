package com.binygal.shabatc.app;

import android.content.*;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v4.view.MotionEventCompat;
import android.util.Log;
import android.view.*;
import android.widget.TextView;
import android.widget.Toast;
import com.binygal.core.messenger.Messenger;
import com.binygal.core.messenger.MessengerListener;
import com.binygal.shabatc.app.common.MessagesNames;
import com.binygal.shabatc.app.model.TimesModel;
import com.binygal.shabatc.app.util.AirplaneModeManager;
import com.binygal.shabatc.app.util.LangaugeManager;
import com.binygal.shabatc.app.util.SystemUiHider;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.binygal.shabatc.app.util.motion.FourPointersHelper;

import java.util.Calendar;
import java.util.Date;


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 *
 * @see SystemUiHider
 */
public class ClockActivity extends Activity {
    private TimesModel model;

    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * If set, will toggle the system UI visibility upon interaction. Otherwise,
     * will show the system UI visibility upon interaction.
     */
    private static final boolean TOGGLE_ON_CLICK = true;

    /**
     * The flags to pass to {@link SystemUiHider#getInstance}.
     */
    private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

    private BroadcastReceiver _broadcastReceiver;

    private AirplaneModeManager _airplaneManager;

    private TextView _clockView;

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getPointerCount() >= 4){
            int action = MotionEventCompat.getActionMasked(event);
            if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN){
                int p1 = MotionEventCompat.getPointerId(event, 0);
                int p2 = MotionEventCompat.getPointerId(event, 1);
                int p3 = MotionEventCompat.getPointerId(event, 2);
                int p4 = MotionEventCompat.getPointerId(event, 3);
                pointersHelper = new FourPointersHelper(p1, p2, p3, p4);
                pointersHelper.setLocation(p1, (int)MotionEventCompat.getY(event, p1));
                pointersHelper.setLocation(p2, (int)MotionEventCompat.getY(event, p2));
                pointersHelper.setLocation(p3, (int)MotionEventCompat.getY(event, p3));
                pointersHelper.setLocation(p4, (int) MotionEventCompat.getY(event, p4));
            } else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP){
                pointersHelper.setEndLocation(pointersHelper.getP1Id(), (int)MotionEventCompat.getY(event, pointersHelper.getP1Id()));
                pointersHelper.setEndLocation(pointersHelper.getP2Id(), (int)MotionEventCompat.getY(event, pointersHelper.getP2Id()));
                pointersHelper.setEndLocation(pointersHelper.getP3Id(), (int)MotionEventCompat.getY(event, pointersHelper.getP3Id()));
                pointersHelper.setEndLocation(pointersHelper.getP4Id(), (int)MotionEventCompat.getY(event, pointersHelper.getP4Id()));
                if (pointersHelper.match()){
                    closeActivity();
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Messenger.getInstance().unregister(this);
        new AirplaneModeManager(this).restoreFromAirplaneMode();
    }

    private FourPointersHelper pointersHelper;

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        model = new TimesModel(this);
        if (!model.setTimes(this)){
            Toast.makeText(this, getResources().getString(R.string.error_no_data), Toast.LENGTH_LONG).show();
            closeActivity();
        }
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_clock);

        _clockView = (TextView)findViewById(R.id.clock_text);
        setTime();

        this._airplaneManager = new AirplaneModeManager(this);
        _airplaneManager.setAirplaneMode();

        setScreenData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setScreenData();
        registerMessages();
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String lang = pref.getString(getResources().getString(R.string.key_language), "");
        if (!lang.equals("")){
            LangaugeManager.setLanguage(this, lang);
        }
    }

    private void registerMessages() {
        Messenger.getInstance().register(this, new MessengerListener() {
            @Override
            public void OnMessageArrived(Object messageData) {
                closeActivity();
            }
        }, MessagesNames.CLOSE_CLOCK);
    }

    private void closeActivity() {
        new AirplaneModeManager(this).restoreFromAirplaneMode();
        setResult(RESULT_OK);
        finish();
    }

    private void setScreenData() {
        TextView shacharitData = (TextView)this.findViewById(R.id.shacharit_content);
        shacharitData.setText(model.getShacharit());
        TextView minchaData = (TextView)this.findViewById(R.id.mincha_content);
        minchaData.setText(model.getMincha());
        TextView arvitData = (TextView)this.findViewById(R.id.arvit_content);
        arvitData.setText(model.getArvit());
        TextView sabbathEnd = (TextView)this.findViewById(R.id.motzei_shabbat_content);
        sabbathEnd.setText(model.getHavdalaTime());
        setTypeface(shacharitData);
        setTypeface(minchaData);
        setTypeface(arvitData);
        setTypeface(sabbathEnd);
        TextView header = (TextView)findViewById(R.id.fullscreen_content);
        setTypeface(header);
        TextView shacharitText = (TextView)findViewById(R.id.shacharit_text);
        TextView minchaText = (TextView)findViewById(R.id.mincha_text);
        TextView arvitText = (TextView)findViewById(R.id.arvit_text);
        TextView havdalaText = (TextView)findViewById(R.id.havdala_text);
        setTypeface(shacharitText);
        setTypeface(minchaText);
        setTypeface(arvitText);
        setTypeface(havdalaText);
    }
//
//    @Override
//    protected void onPostCreate(Bundle savedInstanceState) {
//        super.onPostCreate(savedInstanceState);
//
//        // Trigger the initial hide() shortly after the activity has been
//        // created, to briefly hint to the user that UI controls
//        // are available.
//        delayedHide(100);
//    }

    @Override
    protected void onStart() {
        super.onStart();
        _broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().compareTo(Intent.ACTION_TIME_TICK) == 0){
                    setTime();
                }
            }
        };

        registerReceiver(_broadcastReceiver, new IntentFilter(Intent.ACTION_TIME_TICK));
    }

    private void setTypeface(TextView txt){
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/almoni-tzar.ttf");
        txt.setTypeface(font);
    }

    private void setTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        String  minute = calendar.get(Calendar.MINUTE) < 10 ? "0" + calendar.get(Calendar.MINUTE) : String.valueOf(calendar.get(Calendar.MINUTE));
        String result = calendar.get(Calendar.HOUR_OF_DAY ) + ":" + minute;
        _clockView.setText(result);
        setTypeface(_clockView);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (_broadcastReceiver != null){
            unregisterReceiver(_broadcastReceiver);
        }
    }

//    /**
//     * Touch listener to use for in-layout UI controls to delay hiding the
//     * system UI. This is to prevent the jarring behavior of controls going away
//     * while interacting with activity UI.
//     */
//    View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
//        @Override
//        public boolean onTouch(View view, MotionEvent motionEvent) {
//            if (AUTO_HIDE) {
//                delayedHide(AUTO_HIDE_DELAY_MILLIS);
//            }
//            return false;
//        }
//    };
//
//    Handler mHideHandler = new Handler();
//    Runnable mHideRunnable = new Runnable() {
//        @Override
//        public void run() {
//            mSystemUiHider.hide();
//        }
//    };

//    /**
//     * Schedules a call to hide() in [delay] milliseconds, canceling any
//     * previously scheduled calls.
//     */
//    private void delayedHide(int delayMillis) {
//        mHideHandler.removeCallbacks(mHideRunnable);
//        mHideHandler.postDelayed(mHideRunnable, delayMillis);
//    }
}
