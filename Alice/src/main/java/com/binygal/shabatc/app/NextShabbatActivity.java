package com.binygal.shabatc.app;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;
import com.binygal.shabatc.app.model.TimesModel;


public class NextShabbatActivity extends Activity {

    private TimesModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_shabbat);
        model = new TimesModel(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        model.setTimes(this);
        if (model.getCurrent() == null){
            model.loadTimeServiceData();
            model.setTimes(this);
        }

        TextView candlelightText = (TextView)findViewById(R.id.candlelight_time);
        candlelightText.setText(model.getCurrent().candlelightTime.getReadableTime());
        TextView havdalaText = (TextView)findViewById(R.id.havdala_time);
        havdalaText.setText(model.getCurrent().havdalaTimes.getReadableTime());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_next_shabbat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
