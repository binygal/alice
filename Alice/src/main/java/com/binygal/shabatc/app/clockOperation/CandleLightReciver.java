package com.binygal.shabatc.app.clockOperation;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.binygal.core.messenger.Messenger;
import com.binygal.shabatc.app.ClockActivity;
import com.binygal.shabatc.app.common.MessagesNames;
import com.binygal.shabatc.app.core.hebCalClient.DayTimesData;
import com.binygal.shabatc.app.services.SabbathService;

/**
 * Created by Binyamin on 2/17/2015.
 */
public class CandleLightReciver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        String timeAction = intent.getStringExtra(SabbathService.TIME_ACTION);
        if (timeAction.equals(DayTimesData.HAVDALA)){
            Messenger.getInstance().send(MessagesNames.CLOSE_CLOCK);
        } else if (timeAction.equals(DayTimesData.CANDLELIGHT)){
            Intent i = new Intent(context, ClockActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }
}
