package com.binygal.shabatc.app.common;

import com.binygal.shabatc.app.core.hebCalClient.ShabbatTimes;

/**
 * Created by Binyamin on 2/12/2015.
 */
public interface ITimesService {
    void requestData();

    void clearService();

    void setYear(int year);

    void setMonth(int month);

    void setShabbatTimes(int candlelight, int havdala);

    void setLocation(String city);

    ShabbatTimes getNextOrCurrentShabbatTimes();
}
