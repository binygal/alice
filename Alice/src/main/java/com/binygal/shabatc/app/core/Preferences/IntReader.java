package com.binygal.shabatc.app.core.Preferences;

import android.content.SharedPreferences;

/**
 * Created by Binyamin on 17/03/15.
 */
public class IntReader implements PreferencesReader {
    @Override
    public String getData(SharedPreferences preferences, String key, String defaultValue) {
        String value = String.valueOf(preferences.getInt(key, Integer.parseInt(defaultValue)));
        return value;
    }
}
