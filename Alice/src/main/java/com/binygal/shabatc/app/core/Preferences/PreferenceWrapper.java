package com.binygal.shabatc.app.core.Preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Binyamin on 3/3/2015.
 */
public class PreferenceWrapper {
    public PreferenceWrapper(Context context, Preference preference, String defaultValue){
        this(context, preference, defaultValue, new StringReader());
    }

    public PreferenceWrapper(Context context, Preference preference, String defaultValue, PreferencesReader reader) {
        this.preference = preference;
        this.key = preference.getKey();
        this.defaultValue = defaultValue;
        this.context = context;
        this.reader = reader;
        setSummery();
    }

    private List<String> disableWhenTrueList = new ArrayList<String>();
    private List<String> enableWhenTrueList = new ArrayList<String>();
    public Preference preference;
    private Context context;
    private String defaultValue;
    public String key;
    PreferencesReader reader;

    public void setSummery(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String summery = reader.getData(preferences, key, defaultValue);
        if (!summery.isEmpty()) {
            preference.setSummary(summery);
        }
    }

    public void addTrueEnabledDepended(String key){
        enableWhenTrueList.add(key);
    }

    public void addTrueDisabledDepended(String key){
        disableWhenTrueList.add(key);
    }

    public void raiseKeyChanged(String key){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean val = preferences.getBoolean(key, false);
        for (String s:disableWhenTrueList){
            if (val && s.equals(key)){
                preference.setEnabled(false);
            } else{
                preference.setEnabled(true);
            }
        }
        for (String s:enableWhenTrueList){
            if (val && s.equals(key)){
                preference.setEnabled(true);
            } else {
                preference.setEnabled(false);
            }
        }
    }

}
