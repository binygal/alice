package com.binygal.shabatc.app.core.Preferences;

import android.content.SharedPreferences;

/**
 * Created by Binyamin on 3/4/2015.
 */
public interface PreferencesReader {
    String getData(SharedPreferences preferences, String key, String defaultValue);
}
