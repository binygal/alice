package com.binygal.shabatc.app.core.Preferences;

import android.content.SharedPreferences;

/**
 * Created by Binyamin on 3/4/2015.
 */
public class StringReader implements PreferencesReader {
    @Override
    public String getData(SharedPreferences preferences, String key, String defaultValue) {
        String value = preferences.getString(key, defaultValue);
        return value;
    }
}
