package com.binygal.shabatc.app.core.hebCalClient;

/**
 * Created by Binyamin on 2/15/2015.
 */
public class CalendarData {
    public LocationObject location;
    public String title;
    public String date;
    public String link;
    public DayTimesData[] items;
}
