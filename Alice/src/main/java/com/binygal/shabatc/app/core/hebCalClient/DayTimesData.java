package com.binygal.shabatc.app.core.hebCalClient;

import com.binygal.shabatc.app.util.DateHelper;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Binyamin on 2/17/2015.
 */
public class DayTimesData {
    public static final String CANDLELIGHT = "candles";
    public static final String HAVDALA = "havdalah";
    public String date;
    public String hebrew;
    public String category;
    public String title;

    public String getReadableTime(){
        Date converted = DateHelper.getDateFromString(date);
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(converted);
        return DateHelper.getBeautifyTimeString(calendar);
    }

    public long getTime(){
        Date converted = DateHelper.getDateFromString(date);
        return converted.getTime();
    }
}
