package com.binygal.shabatc.app.core.hebCalClient;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.binygal.shabatc.app.R;
import com.binygal.shabatc.app.common.AppController;
import com.binygal.shabatc.app.common.ITimesService;
import com.binygal.shabatc.app.util.DateHelper;
import com.google.gson.Gson;
import org.json.JSONObject;

import java.io.*;
import java.util.Date;

/**
 * Created by Binyamin on 2/12/2015.
 */
public class HebrewCalTimesService implements ITimesService {
    private final String _baseURI = "http://www.hebcal.com/hebcal/?v=1&cfg=json";

    private String _currentURI = _baseURI;

    private Context _context;

    public HebrewCalTimesService(Context _context) {
        this._context = _context;
    }

    @Override
    public void requestData(){
        JsonObjectRequest req = new JsonObjectRequest(_currentURI, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                String o = jsonObject.toString();
                File file = new File(_context.getFilesDir(), "times");
                if (!file.exists()){
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                FileOutputStream stream;
                try {
                    stream = _context.openFileOutput("times", Context.MODE_PRIVATE);
                    stream.write(o.getBytes());
                    stream.close();
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(_context);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean(_context.getResources().getString(R.string.key_file_prepared), true);
                    editor.commit();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(_context);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(_context.getResources().getString(R.string.key_file_prepared), false);
                editor.commit();
            }
        });
        AppController.getInstance().addToRequestQueue(req);
    }

    @Override
    public void clearService(){
        this._currentURI = _baseURI;
    }

    @Override
    public void setYear(int year){
        String parameterToAdd = String.valueOf(year);
        if (year == 0) {
            parameterToAdd = "now";
        }
        this._currentURI = this._currentURI.concat("&year=" + parameterToAdd);
    }

    @Override
    public void setMonth(int month){
        String parameterToAdd = String.valueOf(month);
        if (month == 0){
            parameterToAdd = "x";
        }
        this._currentURI = this._currentURI.concat("&month=" + parameterToAdd);
    }

    @Override
    public void setShabbatTimes(int candlelight, int havdala){
        this._currentURI = this._currentURI.concat("&c=on&m=" + String.valueOf(havdala));
            this._currentURI = this._currentURI.concat("&b=" + String.valueOf(candlelight));
    }

    @Override
    public void setLocation(String city){
        this._currentURI = this._currentURI.concat("&geo=city");
        this._currentURI = this._currentURI.concat("&city="+city);
    }

    @Override
    public ShabbatTimes getNextOrCurrentShabbatTimes(){
        Gson gson = new Gson();
        File file = new File(_context.getFilesDir(), "times");
        StringBuffer jsonBuffer = new StringBuffer();
        if (!file.exists()){
            Toast.makeText(_context, _context.getResources().getString(R.string.error_no_data), Toast.LENGTH_LONG).show();
            return null;
        }
        try {
            FileInputStream is = _context.openFileInput("times");
            byte[] arr = new byte[1024];
            int numRead = 0;
            while((numRead=is.read(arr)) != -1){
                String readData = new String(arr, 0, numRead);
                jsonBuffer.append(readData);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String jsonResult = jsonBuffer.toString();
        CalendarData data = gson.fromJson(jsonResult, CalendarData.class);
        ShabbatTimes t = new ShabbatTimes();
        for (DayTimesData d:data.items){
            if (d.category.equals(DayTimesData.CANDLELIGHT)){
                Date date = DateHelper.getDateFromString(d.date);
                if(DateHelper.isDateTheCurrentWeekOrRunningRightNow(date)){
                    t.candlelightTime = d;
                }
            }
            if (d.category.equals(DayTimesData.HAVDALA)){
                Date date = DateHelper.getDateFromString(d.date);
                if(DateHelper.isDateTheCurrentWeek(date)){
                    t.havdalaTimes = d;
                }
            }
        }

        return t;
    }

}
