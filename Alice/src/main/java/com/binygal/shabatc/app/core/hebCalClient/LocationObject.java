package com.binygal.shabatc.app.core.hebCalClient;

/**
 * Created by Binyamin on 2/17/2015.
 */
public class LocationObject {
    public String latitude;
    public String geo;
    public String city;
    public String longitude;
    public String title;
    public String tzid;

}
