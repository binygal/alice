package com.binygal.shabatc.app.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import com.binygal.shabatc.app.R;
import com.binygal.shabatc.app.core.Preferences.EmptyReader;
import com.binygal.shabatc.app.core.Preferences.IntReader;
import com.binygal.shabatc.app.core.Preferences.PreferenceWrapper;
import com.binygal.shabatc.app.model.TimesModel;
import com.binygal.shabatc.app.util.DataTools;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Binyamin on 3/1/2015.
 */
public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    private List<PreferenceWrapper> wrappers = new ArrayList<PreferenceWrapper>();
    private List<String> booleanKeys = new ArrayList<String>();
    private TimesModel model;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.times_preferences);
    }

    @Override
    public void onResume() {
        super.onResume();
        model = new TimesModel(getActivity());
        this.setBooleanKeys();
        setWrappers();
        checkBooleanDependencies();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    private void checkBooleanDependencies() {
        for (String b:booleanKeys){
            for (PreferenceWrapper w:wrappers){
                w.raiseKeyChanged(b);
            }
        }
    }

    private PreferenceWrapper getWrapperByKey(String key){
        for (PreferenceWrapper w:wrappers){
            if (w.key == key){
                return w;
            }
        }
        return null;
    }

    private void setWrappers() {
        wrappers.add(new PreferenceWrapper(getActivity(), findPreference(getResources().getString(R.string.shacharit_key)), getResources().getString(R.string.default_time_shacharit)));
        PreferenceWrapper minchaTimeWrapper = new PreferenceWrapper(getActivity(),
                findPreference(getResources().getString(R.string.key_mincha_time)), getResources().getString(R.string.default_time_mincha));
        minchaTimeWrapper.addTrueEnabledDepended(getResources().getString(R.string.key_mincha_fixed));
        wrappers.add(minchaTimeWrapper);
        PreferenceWrapper isMinchaFixedWrapper = new PreferenceWrapper(getActivity(), findPreference(getResources().getString(R.string.key_mincha_before)), String.valueOf(true), new EmptyReader());
        isMinchaFixedWrapper.addTrueDisabledDepended(getResources().getString(R.string.key_mincha_fixed));
        wrappers.add(isMinchaFixedWrapper);
        PreferenceWrapper timeDifferenceMinchaWrapper = new PreferenceWrapper(getActivity(), findPreference(getResources().getString(R.string.mincha_candlelight_difference_key)),
                String.valueOf(0), new IntReader());
        timeDifferenceMinchaWrapper.addTrueDisabledDepended(getResources().getString(R.string.key_mincha_fixed));
        wrappers.add(timeDifferenceMinchaWrapper);
        wrappers.add(new PreferenceWrapper(getActivity(), findPreference(getResources().getString(R.string.key_arvit_before)), String.valueOf(false), new EmptyReader()));
        wrappers.add(new PreferenceWrapper(getActivity(), findPreference(getResources().getString(R.string.key_arvit_havdala_difference)), String.valueOf(0), new IntReader()));
        wrappers.add(new PreferenceWrapper(getActivity(), findPreference(getResources().getString(R.string.key_city)), getResources().getString(R.string.default_city)));
        wrappers.add(new PreferenceWrapper(getActivity(), findPreference(getResources().getString(R.string.key_minutes_candlelight)), getResources().getString(R.string.default_minutes_candlelight), new IntReader()));
        wrappers.add(new PreferenceWrapper(getActivity(), findPreference(getResources().getString(R.string.key_minutes_havdala)), getResources().getString(R.string.default_minutes_havdala), new IntReader()));
        wrappers.add(new PreferenceWrapper(getActivity(), findPreference(getActivity().getResources().getString(R.string.key_language)),""));
    }

    private void setBooleanKeys() {
        booleanKeys.add(getResources().getString(R.string.key_mincha_fixed));
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (booleanKeys.contains(key)) {
            for (PreferenceWrapper w : wrappers) {
                w.raiseKeyChanged(key);
            }
        }

        PreferenceWrapper w = getWrapperByKey(key);
        if (w != null){
            w.setSummery();
        }

        if (key == getResources().getString(R.string.key_city) || key == getResources().getString(R.string.key_minutes_candlelight) || key == getResources().getString(R.string.key_minutes_havdala)){
            model.loadTimeServiceData();
        }

        if (key == getResources().getString(R.string.shacharit_key)) {
            Preference p = findPreference(key);
            String value = sharedPreferences.getString(key, "");
            if (!DataTools.matchTimeString(value)) {
                writeString(key, getResources().getString(R.string.default_time_shacharit), sharedPreferences);
                value = sharedPreferences.getString(key, "");
            }
            p.setSummary(value);
        }
    }

    private void writeString(String key, String value, SharedPreferences preferences){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
}
