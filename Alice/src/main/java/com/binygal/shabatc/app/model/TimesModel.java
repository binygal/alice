package com.binygal.shabatc.app.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.binygal.shabatc.app.R;
import com.binygal.shabatc.app.common.ITimesService;
import com.binygal.shabatc.app.core.hebCalClient.HebrewCalTimesService;
import com.binygal.shabatc.app.core.hebCalClient.ShabbatTimes;
import com.binygal.shabatc.app.util.DateHelper;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Binyamin on 3/4/2015.
 */
public class TimesModel {
    private Context context;

    public ShabbatTimes getCurrent() {
        return current;
    }

    private ShabbatTimes current;
    private ITimesService service;

    public TimesModel(Context context) {
        this.context = context;
        service = new HebrewCalTimesService(context);
    }

    public boolean setTimes(Context context) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        current = service.getNextOrCurrentShabbatTimes();
        if (current == null){
            return false;
        }
        shacharit = pref.getString(getStringFromResources(R.string.shacharit_key), getStringFromResources(R.string.default_time_shacharit));
        mincha = getMinchaTime(pref);
        arvit = getArvitTime(pref);
        havdalaTime = current.havdalaTimes.getReadableTime();
        return true;
    }

    public void loadTimeServiceData(){
        requestTimesFromServer();
    }

    private void requestTimesFromServer() {
        ITimesService times = new HebrewCalTimesService(context);
        times.setYear(0);
        times.setMonth(0);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String city = preferences.getString(context.getResources().getString(R.string.key_city), context.getResources().getString(R.string.default_city));
        times.setLocation(city);
        int candlelightTime = preferences.getInt(context.getResources().getString(R.string.key_minutes_candlelight), 25);
        int havdalaTime = preferences.getInt(context.getResources().getString(R.string.key_minutes_havdala), 35);
        times.setShabbatTimes(candlelightTime, havdalaTime);
        times.requestData();
    }


    private String getArvitTime(SharedPreferences pref) {
        int minuteDifference = pref.getInt(getStringFromResources(R.string.key_arvit_havdala_difference), 0);
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(DateHelper.getDateFromString(current.havdalaTimes.date));
        if (pref.getBoolean(getStringFromResources(R.string.key_arvit_before), false)){
            cal.add(Calendar.MINUTE, -minuteDifference);
        } else {
            cal.add(Calendar.MINUTE, minuteDifference);
        }

        return DateHelper.getBeautifyTimeString(cal);
    }

    private String getMinchaTime(SharedPreferences pref) {
        boolean isFixed = pref.getBoolean(context.getResources().getString(R.string.key_mincha_fixed), true);
        if (isFixed){
            return pref.getString(getStringFromResources(R.string.key_mincha_time), getStringFromResources(R.string.default_time_mincha));
        } else {
            int minuteDifference = pref.getInt(getStringFromResources(R.string.mincha_candlelight_difference_key), 0);
            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(DateHelper.getDateFromString(current.candlelightTime.date));
            if (pref.getBoolean(getStringFromResources(R.string.key_mincha_before), true)){
                cal.add(Calendar.MINUTE, -minuteDifference);
            } else {
                cal.add(Calendar.MINUTE, minuteDifference);
            }

            return DateHelper.getBeautifyTimeString(cal);
        }
    }

    private String getStringFromResources(int key) {
        return context.getResources().getString(key);
    }

    private String shacharit;

    public String getShacharit() {
        return shacharit;
    }

    public String getMincha() {
        return mincha;
    }

    public String getArvit() {
        return arvit;
    }

    public String getHavdalaTime() {
        return havdalaTime;
    }

    private String mincha;
    private String arvit;
    private String havdalaTime;
}
