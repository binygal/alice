package com.binygal.shabatc.app.services;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import com.binygal.shabatc.app.clockOperation.CandleLightReciver;
import com.binygal.shabatc.app.core.hebCalClient.DayTimesData;
import com.binygal.shabatc.app.core.hebCalClient.ShabbatTimes;
import com.binygal.shabatc.app.model.TimesModel;

/**
 * Created by Binyamin on 3/10/2015.
 */
public class SabbathService extends IntentService {
    private static final String TAG = "SabbathService";

    public static final String CREATE = "CREATE";
    public static final String CANCEL = "CANCEL";
    public static final String TIME_ACTION = "TIME_ACTION";
    public static final String TIME = "TIME";

    private IntentFilter matcher;
    private TimesModel model;
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public SabbathService() {
        super(TAG);
        matcher = new IntentFilter();
        matcher.addAction(CREATE);
        matcher.addAction(CANCEL);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        model = new TimesModel(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        String timeAction = intent.getStringExtra(TIME_ACTION);
        long time = intent.getLongExtra(TIME, 0);
        if (matcher.matchAction(action)){
            if (time == 0){
                model.setTimes(this);
                ShabbatTimes current = model.getCurrent();
                execute(action, DayTimesData.CANDLELIGHT, current.candlelightTime.getTime());
                execute(action, DayTimesData.HAVDALA, current.havdalaTimes.getTime());
            } else {
                execute(action, timeAction, time);
            }
        }
    }

    private void execute(String action, String timeAction, long time) {
        AlarmManager am = (AlarmManager)getSystemService(ALARM_SERVICE);
        Intent i = new Intent(this, CandleLightReciver.class);
        i.putExtra(TIME_ACTION, timeAction);
        int requestCode = timeAction.equals(DayTimesData.CANDLELIGHT) ? 0 : 1;
        PendingIntent pi = PendingIntent.getBroadcast(this, requestCode, i, PendingIntent.FLAG_UPDATE_CURRENT);
        if (CREATE.equals(action)){
            if (Build.VERSION.SDK_INT >= 19)
            {
                setExectAlarm(am, time, pi);
            }
            else {
                am.set(AlarmManager.RTC_WAKEUP, time, pi);
            }
        } else if (CANCEL.equals(action)){
            am.cancel(pi);
        }
    }

    @TargetApi(19)
    private void setExectAlarm(AlarmManager am, long time, PendingIntent pi) {
        am.setExact(AlarmManager.RTC_WAKEUP, time, pi);
    }
}
