package com.binygal.shabatc.app.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Binyamin on 3/10/2015.
 */
public class SabbathSetter extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, SabbathService.class);
        service.setAction(SabbathService.CREATE);
        context.startService(service);
    }
}
