package com.binygal.shabatc.app.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.WindowManager;
import android.widget.Toast;
import com.binygal.shabatc.app.R;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Binyamin on 2/9/2015.
 */
public class AirplaneModeManager {
    private final Context _context;

    public AirplaneModeManager(Context context) {
        this._context = context;
    }

    public void setAirplaneMode(){
        toggleWifi(false);
        toggleMobileData(false);
        toggleBluetooth(false);
        toggleRingerMode(AudioManager.RINGER_MODE_SILENT);
        toggleDimScreen(1);

        Toast.makeText(this._context,"Shabat Shalom! All connectivity disabled", Toast.LENGTH_LONG).show();
    }

    public void toggleRingerMode(int ringerMode){
        AudioManager amanager = (AudioManager) _context.getSystemService(Context.AUDIO_SERVICE);
        int mode = amanager.getRingerMode();
        if (mode != ringerMode){
            if (ringerMode == AudioManager.RINGER_MODE_SILENT){
                writeToPreferences(_context.getResources().getString(R.string.key_audio_mode), mode);
            }
            amanager.setRingerMode(ringerMode);
        }
    }

    public void toggleDimScreen(int brightnessValue){
        if (brightnessValue == 1){
            try{
                Activity context = (Activity)_context;
                WindowManager.LayoutParams lp = context.getWindow().getAttributes();
                int i = (int)lp.screenBrightness;
                writeToPreferences(_context.getResources().getString(R.string.key_brightness), i);
                lp.screenBrightness = brightnessValue;
                context.getWindow().setAttributes(lp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try{
                Activity context = (Activity)_context;
                WindowManager.LayoutParams lp = context.getWindow().getAttributes();
                lp.screenBrightness = brightnessValue;
                context.getWindow().setAttributes(lp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void restoreFromAirplaneMode(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(_context);
        if (sharedPreferences.getBoolean(_context.getResources().getString(R.string.key_wifi), false)){
            toggleWifi(true);
        }
        if (sharedPreferences.getBoolean(_context.getResources().getString(R.string.key_mobile_data), true)){
            toggleMobileData(true);
        }
        if (sharedPreferences.getBoolean(_context.getResources().getString(R.string.key_bluetooth), false)){
            toggleBluetooth(true);
        }
        int ringerMode = sharedPreferences.getInt(_context.getResources().getString(R.string.key_audio_mode), AudioManager.RINGER_MODE_NORMAL);
        toggleRingerMode(ringerMode);
        int brightnessVal = sharedPreferences.getInt(_context.getResources().getString(R.string.key_brightness), 200);
        toggleDimScreen(brightnessVal);
    }

    private void toggleWifi(boolean newState){
        WifiManager wifiManager = (WifiManager)this._context.getSystemService(Context.WIFI_SERVICE);
        if (newState && !wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(true);
        } else if (!newState && wifiManager.isWifiEnabled()){
            writeToPreferences(_context.getResources().getString(R.string.key_wifi), true);
            wifiManager.setWifiEnabled(false);
        } else if (!newState && !wifiManager.isWifiEnabled()){
            writeToPreferences(_context.getResources().getString(R.string.key_wifi), false);
        }
    }

    private void toggleMobileData(boolean newState) {
        ConnectivityManager connectivityManager = (ConnectivityManager)this._context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (!newState){
            if (getMobileDataState(connectivityManager)){
                writeToPreferences(_context.getResources().getString(R.string.key_mobile_data), true);
            } else {
                writeToPreferences(_context.getResources().getString(R.string.key_mobile_data), false);
            }
        }
        Class conManType = null;
        try {
            conManType = Class.forName(connectivityManager.getClass().getName());
        Field connectivityManagerField = conManType.getDeclaredField("mService");
        connectivityManagerField.setAccessible(true);
        Object iConnectivityManager = connectivityManagerField.get(connectivityManager);
        Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
        Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);
        setMobileDataEnabledMethod.invoke(iConnectivityManager, newState);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private boolean getMobileDataState(ConnectivityManager cm){
        Class<?> c = null;
        try {
            c = Class.forName(cm.getClass().getName());

        Method m = c.getDeclaredMethod("getMobileDataEnabled");
        m.setAccessible(true);
        return (Boolean)m.invoke(cm);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void toggleBluetooth(boolean newState){
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null && Build.VERSION.SDK_INT >= 18){
            bluetoothAdapter = getBluetoothAdapter();
        }

        if (bluetoothAdapter == null){
            return;
        }
        if (bluetoothAdapter.isEnabled() && !newState){
            bluetoothAdapter.disable();
            writeToPreferences(_context.getResources().getString(R.string.key_bluetooth), true);
        } else if (!bluetoothAdapter.isEnabled() && newState){
            bluetoothAdapter.enable();
        } else if (!bluetoothAdapter.isEnabled() && !newState){
            writeToPreferences(_context.getResources().getString(R.string.key_bluetooth), false);
        }
    }

    @TargetApi(18)
    private BluetoothAdapter getBluetoothAdapter() {
        BluetoothManager bluetooth = (BluetoothManager)_context.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = bluetooth.getAdapter();
        return bluetoothAdapter;
    }

    private void writeToPreferences(String key, boolean value){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(_context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    private void writeToPreferences(String key, int value){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(_context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }
}
