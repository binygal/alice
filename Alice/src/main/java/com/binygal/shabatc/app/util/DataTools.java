package com.binygal.shabatc.app.util;

/**
 * Created by Binyamin on 3/2/2015.
 */
public class DataTools {
    public static boolean matchTimeString(String checkedString){
        String regex = "[0-2]?[0-9]:[0-5][0-9]";
        if (checkedString.matches(regex)){
            String[] splited = checkedString.split(":");
            if (Integer.parseInt(splited[0]) < 24) {
                return true;
            }
        }
        return false;
    }
}
