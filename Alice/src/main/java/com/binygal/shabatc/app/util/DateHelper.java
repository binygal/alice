package com.binygal.shabatc.app.util;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by Binyamin on 3/1/2015.
 */
public class DateHelper {
    public static Date getDateFromString(String value){
        GregorianCalendar cal = new GregorianCalendar();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date res = format.parse(value, new ParsePosition(0));
        return res;
    }

    public static boolean isDateTheCurrentWeek(Date queried){
        return checkDaysDifference(queried, 7);
    }

    public static boolean isDateTheCurrentWeekOrRunningRightNow(Date queried){
        return checkDaysDifference(queried, 6);
    }

    private static boolean checkDaysDifference(Date queried, int daysToCheck){
        GregorianCalendar today = new GregorianCalendar();
        Date todayDate = today.getTime();
        long difference = todayDate.getTime() - queried.getTime();
        long days = TimeUnit.MILLISECONDS.toDays(Math.abs(difference));
        if (difference < 0 && days < daysToCheck)
        {
            return true;
        }

        return false;
    }

    public static String getBeautifyTimeString(Calendar cal){
        String minutes = cal.get(Calendar.MINUTE) < 10 ? "0" + cal.get(Calendar.MINUTE) : String.valueOf(cal.get(Calendar.MINUTE));
        return String.valueOf(cal.get(Calendar.HOUR_OF_DAY) + ":" + minutes);
    }
}
