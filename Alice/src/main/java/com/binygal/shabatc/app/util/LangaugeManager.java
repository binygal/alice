package com.binygal.shabatc.app.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

/**
 * Created by Binyamin on 17/03/15.
 */
public class LangaugeManager {
    public static void setLanguage(Activity context, String localeName){
        if (localeName == context.getResources().getConfiguration().locale.getLanguage()){
            return;
        }
        Locale locale = new Locale(localeName);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getBaseContext().getResources().updateConfiguration(config,
                context.getBaseContext().getResources().getDisplayMetrics());
        context.recreate();
    }
}
