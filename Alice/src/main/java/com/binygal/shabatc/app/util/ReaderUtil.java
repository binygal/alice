package com.binygal.shabatc.app.util;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Binyamin on 16/03/15.
 */
public class ReaderUtil {
    public static String readRawTextFile(Context context, int resId) {
        InputStream inputStream = context.getResources().openRawResource(resId);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder text = new StringBuilder();
        try {
            String line = reader.readLine();
            while (line != null) {
                text.append(line);
                text.append('\n');
                line = reader.readLine();
            }
        } catch (IOException e) {
            return null;
        }
        return text.toString();
    }
}
