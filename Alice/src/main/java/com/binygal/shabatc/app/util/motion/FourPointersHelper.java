package com.binygal.shabatc.app.util.motion;

/**
 * Created by Binyamin on 3/12/2015.
 */
public class FourPointersHelper {
    public int getP1Id() {
        return p1Id;
    }

    public int getP2Id() {
        return p2Id;
    }

    public int getP3Id() {
        return p3Id;
    }

    public int getP4Id() {
        return p4Id;
    }

    private int p1Id;

    private int p2Id;

    private int p3Id;

    private int p4Id;

    private int p1Loc;

    private int p2Loc;

    private int p3Loc;

    public int getP1Loc() {
        return p1Loc;
    }

    public int getP1EndLoc() {
        return p1EndLoc;
    }

    private int p4Loc;

    private int p1EndLoc;

    private int p2EndLoc;

    private int p3EndLoc;

    private int p4EndLoc;


    public FourPointersHelper(int p1Id, int p2Id, int p3Id, int p4Id) {
        this.p1Id = p1Id;
        this.p2Id = p2Id;
        this.p3Id = p3Id;
        this.p4Id = p4Id;
    }

    public void setLocation(int id, int location){
        if (id == p1Id){
            p1Loc = location;
        }
        if (id == p2Id){
            p2Loc = location;
        }
        if (id == p3Id){
            p3Loc = location;
        }
        if (id == p4Id){
            p4Loc = location;
        }
    }

    public void setEndLocation(int id, int location){
        if (id == p1Id){
            p1EndLoc = location;
        }
        if (id == p2Id){
            p2EndLoc = location;
        }
        if (id == p3Id){
            p3EndLoc = location;
        }
        if (id == p4Id){
            p4EndLoc = location;
        }
    }

    public boolean match(){
        if (p1EndLoc - p1Loc > 10 && p2EndLoc - p2Loc > 10  && p3EndLoc - p3Loc > 10  && p4EndLoc - p4Loc > 10){
            return true;
        }
        return false;
    }
}
